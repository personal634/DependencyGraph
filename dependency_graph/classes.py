"""Includes dependency-graph classes."""
import os
import re
from dataclasses import dataclass
from os import walk
from pathlib import Path
from typing import Iterable, List, Optional, Set, Union


PathTypes = Optional[Union[str, Iterable[str], Path, Iterable[Path]]]


@dataclass
class DepInfo:
    """Class containing import information.

    from_statement : Optional[str]
        Found dependency was in the form of *from X import Y*.
    import_statement : Optional[str]
        Found dependency was in the form of *import X*.
    imported_object_name : str
        The imported dependency.
    imported_object_is_module : bool

    """
    from_statement: Optional[str]
    import_statement: Optional[str]
    imported_object_name: str
    imported_object_is_module: bool
    dependency_main_module_name: str
    dependency_main_module_path: Optional[Union[str, Path]]
    dependency_last_module_name: Optional[str]
    dependency_last_module_path: Optional[Union[str, Path]]
    pass


class DependencyGraph(object):
    """Build the dependency graph object.

    Constructor examines ``self.filename`` Python script's dependencies in
    order to build the dependency digraph.

    :param filename: path to Python script to analyze.
    :type filename: str
    :param search_paths: optional search paths.
    :type search_paths: path_types
    :raises: FileNotFoundError
    :ivar root: initial value: filename.
    :type root: str
    """

    """Dependency graph object.

    Class/Structure containing relevant information for building the
    dependency digraph of the Python project.

    'filename' is the path to the Python source file or Python package
    directory.

    'search_paths' includes the directory or list of directories where package
    dependencies will be searched in addition to 'filename' directory.
    """

    def __init__(
        self,
        filename: Union[str, Path],
        search_paths: PathTypes = None,
        skip_paths: Optional[List[Path]] = None,
    ):

        if isinstance(filename, str) and filename.startswith("external:"):
            self.root: str = filename.split(":", 1)[-1]
            self.root_filename: str = self.root
            self._is_package: bool = True
            self.search_paths: List[str] = list()
            self.module_deps: Set[DependencyGraph] = set()
            self.module_names_v2 = list()
            self.module_paths_v2 = list()
            self.module_deps_v2 = list()

            pass
        else:
            cwd = Path(filename)
            self.root_filename: Path = cwd.absolute()

            if cwd.is_file():
                # root = "/".join(cwd.as_posix().split("/")[:-1])
                self.root: Path = cwd.parent.absolute()
                self._is_package: bool = False
            elif self.is_package(str(filename)):
                self.root: Path = cwd.absolute()
                self._is_package: bool = True
                pass
            else:
                msg = "Error, file is nor package nor file: {}"
                print(msg.format(filename))
                raise FileNotFoundError()
                pass

            self.search_paths: List[Path] = [self.root]
            if search_paths is None:
                pass
                # self.search_paths = (self.root,)
            elif type(search_paths) is str:
                if Path(search_paths).exists():
                    self.search_paths.append(Path(search_paths))
                    pass
            else:
                for s in search_paths:
                    if Path(s).exists():
                        self.search_paths.append(Path(s))
                        pass
                    pass
                pass

            # self.module_names = self.inspect_module_names()
            self.module_names_v2 = self.inspect_deps()
            # self.module_paths = self.get_module_paths()
            self.module_paths_v2 = self.get_module_paths_v2()
            # self.module_deps = self.get_module_deps()
            self.module_deps_v2 = self.get_module_deps_v2(skip_paths)

            """
            self.subs = []
            for dep in self.module_names_v2:
                if dep.dependency_main_module_path is not None:
                    self.subs.append(
                        DependencyGraph(
                            dep.dependency_main_module_path,
                            self.search_paths
                        )
                    )
                    pass
                else:
                    self.subs.append(dep.dependency_main_module_name)
                    pass"""
            del cwd
            pass
        pass

    def inspect_deps(self) -> List[DepInfo]:
        """Get imported modules.

        Examines ``self.filename`` in search of imported packages and returns
        a set of found module names.

        :return: Set of imported module names.
        :rtype: Set[DepInfo]
        """
        modules = []
        pattern_1 = r"^import\s+(?P<modules>.*)$"
        pattern_3 = r"^from\s+(?P<src>[.\w]+)\s+import\s+(?P<modules>.*)$"

        def gen_var(match: re.Match):
            mods: str = match.group("modules")
            for modd in mods.split(","):
                mm = modd.strip()
                mm = re.sub(r"\s+as\s+.*", "", mm)
                yield mm
            pass

        if self.root_filename.exists():
            with open(str(self.root_filename), "r") as file:
                for line in file.readlines():
                    m = re.match(pattern_1, line)
                    if m:
                        for module in gen_var(m):
                            modules.append(
                                self.eval_mod(
                                    root=self.root_filename,
                                    search_paths=self.search_paths,
                                    module=module,
                                )
                            )
                            pass
                        pass
                    m = re.match(pattern_3, line)
                    if m:
                        trail = m.group("src")
                        for module in gen_var(m):
                            modules.append(
                                self.eval_mod(
                                    root=self.root_filename,
                                    search_paths=self.search_paths,
                                    module=module,
                                    trail=trail,
                                )
                            )
                            pass
                        pass
                    pass
                pass
            pass
        return modules

    @staticmethod
    def import_path(r: Path, a: str) -> Optional[Path]:
        # Module
        m = r.joinpath(a + ".py")
        # Package
        p = r.joinpath(a).joinpath("__init__.py")
        if m.exists():
            return m
        elif p.exists():
            return p
        else:
            return None
        pass

    pass

    @staticmethod
    def eval_mod(
        root: Path,
        search_paths: List[Path],
        module: str,
        trail: Optional[str] = None,
    ) -> DepInfo:
        # Evaluate simple 'import a'
        if trail is None and "." not in module:
            for s in search_paths:
                p = DependencyGraph.import_path(s, module)
                if p is not None:
                    return DepInfo(
                        from_statement=None,
                        import_statement=module,
                        imported_object_name="*",
                        imported_object_is_module=True,
                        dependency_main_module_name=module,
                        dependency_main_module_path=p,
                        dependency_last_module_name=module,
                        dependency_last_module_path=p,
                    )
                pass
            return DepInfo(
                from_statement=None,
                import_statement=module,
                imported_object_name="*",
                imported_object_is_module=False,
                dependency_main_module_name=module,
                dependency_main_module_path=None,
                dependency_last_module_name=module,
                dependency_last_module_path=None,
            )
        # Evaluate submodule 'import a.b'
        elif trail is None and "." in module:
            for ss in search_paths:
                s = Path(ss)
                for d in module.split(".")[:-1]:
                    s = s.joinpath(d)
                    pass
                p0 = DependencyGraph.import_path(ss, module.split(".")[0])
                p = DependencyGraph.import_path(s, module.split(".")[-1])
                if p is not None and p0 is not None:
                    return DepInfo(
                        from_statement=None,
                        import_statement=module,
                        imported_object_name="*",
                        imported_object_is_module=True,
                        dependency_main_module_name=module.split(".")[0],
                        dependency_main_module_path=p0,
                        dependency_last_module_name=module.split(".")[-1],
                        dependency_last_module_path=p,
                    )
                pass
            return DepInfo(
                from_statement=None,
                import_statement=module,
                imported_object_name="*",
                imported_object_is_module=False,
                dependency_main_module_name=module.split(".")[0],
                dependency_main_module_path=None,
                dependency_last_module_name=module.split(".")[-1],
                dependency_last_module_path=None,
            )
        # Evaluate from 'from (.|..)x.y import (a.b|X)'
        else:
            s = trail
            n = 0
            # print(s)
            while s.startswith("."):
                n = n + 1
                s = s[1:]
                pass
            # print("N, S:\t", n, s)
            for sp in search_paths:
                # print("SP:\t", sp)
                new_root = Path(str(sp))
                if n > 1:
                    new_root = new_root.parents[n - 2]
                    pass
                # print("Path:\t", new_root)
                p0 = DependencyGraph.import_path(new_root, s.split(".")[0])
                # print("P0:\t", p0)
                if p0 is None:
                    continue
                    pass
                from_path = new_root
                for d in s.split("."):
                    from_path = from_path.joinpath(d)
                    pass
                if "." in module:
                    for d in module.split("."):
                        from_path = from_path.joinpath(d)
                        pass
                    p = DependencyGraph.import_path(from_path, "")
                    # print("P:\t", p)
                    return DepInfo(
                        from_statement=trail,
                        import_statement=module,
                        imported_object_name=module.split(".")[-1],
                        imported_object_is_module=True,
                        dependency_main_module_name=s.split(".")[0],
                        dependency_main_module_path=p0,
                        dependency_last_module_name=module.split(".")[-1]
                        if "." in module
                        else trail.split(".")[-1],
                        dependency_last_module_path=p,
                    )
                else:
                    p = DependencyGraph.import_path(from_path, module)
                    print("TRAIL:       ", trail)
                    print("MODULE:      ", module)
                    print("FROM PATH:   ", from_path)
                    print("PATH:        ", p)
                    print(" ")
                    if p is None:
                        print("FROM PATH:\t", from_path)
                        p = DependencyGraph.import_path(from_path, "")
                        if p is None:
                            p = Path(str(from_path) + ".py")
                            p = p if p.exists() else None
                            pass
                        print("P:\t", p)
                        mn = s.split(".")[0]
                        mn = module if mn == "" else mn
                        return DepInfo(
                            from_statement=trail,
                            import_statement=module,
                            imported_object_name=module,
                            imported_object_is_module=True,
                            dependency_main_module_name=mn,
                            dependency_main_module_path=p0,
                            dependency_last_module_name=module.split(".")[-1]
                            if "." in module
                            else trail.split(".")[-1],
                            dependency_last_module_path=p,
                        )
                    else:
                        mn = s.split(".")[0]
                        mn = module if mn == "" else mn
                        return DepInfo(
                            from_statement=trail,
                            import_statement=module,
                            imported_object_name=module,
                            imported_object_is_module=True,
                            dependency_main_module_name=mn,
                            dependency_main_module_path=p0,
                            dependency_last_module_name=module.split(".")[-1]
                            if "." in module
                            else trail.split(".")[-1],
                            dependency_last_module_path=p,
                        )
                pass
            return DepInfo(
                from_statement=trail,
                import_statement=module,
                imported_object_name=module.split(".")[-1],
                imported_object_is_module=False,
                dependency_main_module_name=s.split(".")[0],
                dependency_main_module_path=None,
                dependency_last_module_name=module.split(".")[-1]
                if "." in module
                else trail.split(".")[-1],
                dependency_last_module_path=None,
            )
            pass
        pass

    def get_module_paths_v2(self) -> Set[str]:
        """Get imported modules' paths.

        Examines the found module names from 'inspect_module_names' and
        returns a set of paths where those modules are defined.

        :returns: set of file/package paths where imported modules are defined.
        :rtype: Set[str]
        """
        paths = []
        for module in self.module_names_v2:
            if module.dependency_main_module_path is None:
                paths.append("external:{}".format(module.imported_object_name))
                pass
            else:
                paths.append(module.dependency_main_module_path)
                pass
            pass
        return set(paths)

    def get_module_deps_v2(
        self, c: Optional[List[Path]]
    ) -> List[Union[str, "DependencyGraph"]]:
        """Get imported modules' dependancy graph objects.

        Generates a set of DependencyGraph objects of the found modules'
        paths from 'get_module_paths'.

        :returns: Set of dependencies as DependencyGraph objects.
        :rtype: Set["DependencyGraph"]
        """
        if c is None:
            c = []
            pass
        print("C: ", c)

        # return self.module_names_v2
        deps = []
        for module in self.module_names_v2:
            print(module)
            if module.imported_object_is_module:
                if module.dependency_last_module_path is None:
                    deps.append(
                        DependencyGraph(
                            "external:" + module.imported_object_name
                        )
                    )
                    pass
                elif module.dependency_last_module_path not in c:
                    # else:
                    c.append(module.dependency_last_module_path)
                    deps.append(
                        DependencyGraph(
                            module.dependency_last_module_path,
                            search_paths=self.search_paths,
                            skip_paths=c,
                        )
                    )
                    pass
                pass
            elif module.dependency_last_module_path is not None:
                c.append(module.dependency_last_module_path)
                deps.append(
                    DependencyGraph(
                        module.dependency_last_module_path,
                        search_paths=self.search_paths,
                        skip_paths=c,
                    )
                )
            pass
        return deps

    @staticmethod
    def is_package(abs_path: str) -> bool:
        """Get True if 'abs_path' is package directory, else False.

        :param abs_path: path to the potential package directory.
        :type abs_path: str
        :return: True if ``abs_path`` is package dir.
        :rtype: bool
        """
        ans = False
        path = Path(abs_path)
        if path.is_dir():
            init = path.joinpath("__init__.py")
            if init.is_file():
                ans = True
                pass
            pass
        return ans

    def __str__(self) -> str:
        """Get string representation of the dependency graph.

        :return: String representation of the dependency graph.
        """
        text = "Root Filename: {}\n".format(self.root_filename)
        try:
            for dep in self.module_deps:
                text += "\t" + str(dep)
                pass
            pass
        except Exception:
            for dep in self.module_names_v2:
                text += "\t" + str(dep) + "\n"
                pass
            pass
        return text

    pass


__all__ = [DependencyGraph, DepInfo]
