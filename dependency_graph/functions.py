"""Includes dependency-graph functions."""
from os import sep
from pathlib import Path
from typing import List, Optional, Tuple

from pydotplus import Dot, Edge, Node, Subgraph

from .classes import DependencyGraph as Dg, DepInfo


# from typing import List

PKG_SUFFIX: str = " (pkg)"
MAX_STR_LEN = 40


def build_v1(graph: Dg) -> Subgraph:
    """Generate the dependency digraph.

    Builds the dependency digraph recursively from the main node.

    :param graph: main node.
    :type graph: Dg
    :return: Dot type digraph.
    :rtype: Dot
    """
    # Initializations.
    digraph = Subgraph()

    # Node naming.
    main_node_name = str(graph.root_filename).split(sep)[-1]
    if main_node_name.endswith("__init__.py"):
        main_node_name = str(graph.root_filename).split(sep)[-2] + PKG_SUFFIX
        pass

    # Main node.
    main_node = Node(main_node_name)
    configure_node(main_node, graph.module_deps)
    digraph.add_node(main_node)

    # Main node deps.
    for dep in graph.module_deps:

        # Dep node naming.
        node_name = str(dep.root_filename).split(sep)[-1]
        if node_name.endswith("__init__.py"):
            node_name = str(dep.root_filename).split(sep)[-2] + PKG_SUFFIX
            pass

        # Dep node creation.
        node = Node(node_name)
        edge = Edge(main_node_name, node_name)
        digraph.add_node(node)

        # Create Edge.
        digraph.add_edge(edge)

        # Recursion: Dependency's deps.
        digraph.add_subgraph(build(dep))
        pass

    # Delete multiple edges on common dependency.
    clean_edges(digraph)
    return digraph


def build(graph: Dg, is_top: Optional[bool] = True) -> Subgraph:
    """Generate the dependency digraph.

    Builds the dependency digraph recursively from the main node.

    :param is_top: do not change, internal-use flag.
    :type is_top: Optional[bool]
    :param graph: main node.
    :type graph: Dg
    :return: Dot type digraph.
    :rtype: Dot
    """
    # Initializations.
    digraph = Subgraph()

    # Node naming.
    main_node_name = Path(graph.root_filename).name
    if main_node_name.endswith("__init__.py"):
        main_node_name = Path(
            graph.root_filename
        ).parent.name  # str(graph.root_filename).split(sep)[-2]  #  + PKG_SUFFIX
        pass
    elif main_node_name.endswith(".py"):
        main_node_name = graph.root_filename.name[:-3]
        pass

    # Main node.
    main_node = Node(main_node_name)
    configure_node_v2(main_node, graph.module_names_v2)
    digraph.add_node(main_node)

    # Main node deps.
    # assert len(graph.module_names_v2) == len(graph.module_deps_v2)
    for dep in graph.module_names_v2:

        # Dep node naming.
        node_name = dep.dependency_main_module_name
        if node_name.endswith("__init__.py"):
            node_name = (
                dep.dependency_main_module_path.parent.name
            )  # + PKG_SUFFIX
            pass
        elif node_name.endswith(".py"):
            node_name = dep.dependency_main_module_path.name[:-3]
            pass

        # Dep node creation.
        node = Node(node_name)
        edge = Edge(main_node_name, node_name)
        digraph.add_node(node)

        # Create Edge.
        digraph.add_edge(edge)

        # Recursion: Dependency's deps.
        # print(sg)
        pass
    print(graph.module_deps_v2)
    for sg in graph.module_deps_v2:
        digraph.add_subgraph(build(sg, is_top=False))
        pass

    # Delete multiple edges on common dependency.
    if is_top:
        clean_edges(digraph)
    is_top = False
    if is_top:
        nodes: List[Node] = []
        edges: List[Edge] = []
        get_nodes_edges(digraph, nodes, edges)
        [print(n.get_name(), n.get("shape")) for n in nodes]
        print(edges)
        nsg = Subgraph()
        for n in nodes:
            nsg.add_node(n)
            pass
        [nsg.add_edge(e) for e in edges]
        clean_edges(nsg)
        return nsg
        # clean_edges(digraph)
        pass
    return digraph


def get_nodes_edges(sg: Subgraph, nodes: List[Node], edges: List[Edge]):
    """Get edges linking nodes.

    :param sg: Current subgraph.
    :type sg: Subgraph
    :param nodes: list of nodes.
    :type nodes: List[Node]
    :param edges: in-out list of edges.
    :type edges: List[Edge]
    """
    for node in sg.get_nodes():
        nodes.append(node)
        pass
    for edge in sg.get_edges():
        edges.append(edge)
        pass
    nodes = list(set(nodes))
    edges = list(set(edges))

    for ssgs in sg.get_subgraphs():
        get_nodes_edges(ssgs, nodes, edges)
    pass


def configure_node(node: Node, deps: List[Dg]) -> None:
    """Define the node view on the graph tree.

    :param node: current node.
    :type node: Node
    :param deps: current node dependencies.
    :type deps: List[Dg]
    """
    if len(deps) > 0:
        node.set("shape", "record")
        label: str = "{" + node.get_name().replace('"', "") + "|"
        label = (
            label
            + "{ Deps | {"
            + "|".join(str(d.root).split("/")[-1] for d in deps)
            + "}}"
        )
        label = label + "}"
        node.set("label", label)
        pass
    else:
        pass
    pass


def configure_node_v2(node: Node, deps: List[DepInfo]) -> None:
    """New way of defining the node view on the graph tree.

    :param node: current node.
    :type node: Node
    :param deps: current node dependencies.
    :type deps: List[DepInfo]
    """
    if len(deps) > 0:
        node.set("shape", "record")
        src = ["Source"] + [d.dependency_main_module_name for d in deps]
        var = ["Import"] + [d.imported_object_name for d in deps]

        # label: str = "{" + node.get_name().replace('"', '') + '|'
        # label = label + '{{{' + '|'.join(src) + '}}|{{' + '|'.join(var) + '}}}'
        # label = label + '}'

        label = (
            node.get_name().replace('"', "")
            + "|"
            + "{"
            + "|".join(src)
            + "}"
            + "|"
            + "{"
            + "|".join(var)
            + "}"
        )
        node.set("label", label)
        node.set("rankdir", "LR")
        pass
    else:
        pass
    pass


def get_all_edges(sg: Subgraph, l=None) -> List[Tuple[Subgraph, Edge]]:
    """Get the edges of a subgraph.

    :param sg: current subgraph.
    :type sg: Subgraph
    :param l: list of edges, initialized as None and filled recursively.
    :type l: NoneType
    :rtype: List[Tuple[Subgraph, Edge]]
    """
    if l is None:
        l = []
    l = l + [(sg, e) for e in sg.get_edge_list()]
    for ssg in sg.get_subgraph_list():
        l = get_all_edges(ssg, l)
        pass
    return l


def clean_edges(sg: Subgraph):
    """Clean duplicate edges.

    :param sg: current subgraph.
    :type sg: Subgraph
    """
    # All edges.
    edges = get_all_edges(sg)

    # Backup edges.
    existing_edge_params: List[Tuple[Subgraph, str, str]] = []
    edgs: List[Tuple[str, str]] = []
    for ssg, e in edges:
        edgs.append((e.get_source(), e.get_destination()))
        ssg.del_edge(e.get_source(), e.get_destination())
        if any(
            [
                s == e.get_source() and d == e.get_destination()
                for _, s, d in existing_edge_params
            ]
        ):
            # Edge already exists.
            ssg.del_edge(e.get_source(), e.get_destination())
            pass
        else:
            # Edge does not exist, backup.
            existing_edge_params.append(
                (ssg, e.get_source(), e.get_destination())
            )
            pass
        pass
    edgs = list(set(edgs))
    for s, d in edgs:
        sg.add_edge(Edge(s, d))
    for ssg in sg.get_subgraph_list():
        clean_edges(ssg)
    pass


def export(digraph: Subgraph, name: str = "a") -> None:
    """Generate the dependency digraph plot.

    Exports the generated digraph from 'build' to 'name'.png file.

    :param digraph: Dot digraph from 'build'.
    :type digraph: Dot
    :param name: export file's basename.
    :type name: str
    """
    graph = Dot()
    graph.add_subgraph(digraph)
    graph.write(name + ".dot")
    result = graph.create(format="png")
    if result is None:
        print("Error!")
        pass
    else:
        with open(name + ".png", "wb") as file:
            file.write(result)
            pass
        pass
    pass
