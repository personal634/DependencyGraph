"""Test the Python script parser to ensure that dependencies are correctly
detected.

The test is made against the scripts on ``tests/``, as such the expected
dependencies for all (or part) of the scripts is defined on the
``EXPECTED_DEPS`` variable.
"""
import os
from os import pardir
from pathlib import Path

from typing import Dict, List

import dependency_graph as dg
from dependency_graph.classes import DepInfo
from dependency_graph import __version__

CURRENT_DIR: str = os.path.dirname(__file__)
TEST_CASES: str = "test_cases"
TEST_CASES_DIR: str = os.path.join(CURRENT_DIR, TEST_CASES)
EXPECTED_DEPS: Dict[str, List[DepInfo]] = {
    "simple_import.py": [
        DepInfo(
            from_statement=None,
            import_statement="os",
            imported_object_name="*",
            imported_object_is_module=False,
            dependency_main_module_name="os",
            dependency_main_module_path=None,
            dependency_last_module_name="os",
            dependency_last_module_path=None
        ),
        DepInfo(
            from_statement=None,
            import_statement="enum",
            imported_object_name="*",
            imported_object_is_module=False,
            dependency_main_module_name="enum",
            dependency_main_module_path=None,
            dependency_last_module_name="enum",
            dependency_last_module_path=None
        )
    ]
}


def test_parser():
    """Test parser by checking test case scripts.

    Test the scripts on ``TEST_CASES_DIR`` and assert that the found
    dependencies match the expected ones.
    """
    for script_name, expected_deps in EXPECTED_DEPS.items():
        file_path = os.path.join(TEST_CASES_DIR, script_name)
        graph = dg.DependencyGraph(file_path)
        found_deps: List[DepInfo] = graph.module_names_v2
        assert len(found_deps) == len(expected_deps)

        print("FOUND DEPS:\n", found_deps, "\n" + "-"*80)

        for c_dep in found_deps:
            matches = 0
            for i_dep in expected_deps:
                if c_dep == i_dep:
                    matches = matches + 1
                    pass
                pass

            assert matches == 1

        assert all([fd == ed for fd, ed in zip(found_deps, expected_deps)])
        pass
    pass
