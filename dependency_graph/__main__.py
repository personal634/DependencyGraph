"""Dependency graph CLI using Click."""

from pathlib import Path
from typing import List

import click
from pydotplus import Dot

from dependency_graph import DependencyGraph, build, export


# diag = DependencyGraph(
#     "us2_environment",
#     [
#         "us2_environment",
#         "us2_environment/earth_magneticfield"
#     ]
# )
# digraph = build(diag)
# export(digraph, "diagrams/diag")


@click.command()
@click.option(
    "--out",
    "-o",
    default=None,
    type=click.Path(),
    required=False,
    help="Graph output filename without extension.",
)
@click.option(
    "--paths",
    "-p",
    default=None,
    multiple=True,
    type=click.Path(),
    required=False,
    help='Additional search paths separated by colons ":" (Unix) or '
    'semicolons ";" (Windows).',
)
@click.argument("path", type=click.Path(exists=True, readable=True))
def cli(out: str, paths: List[click.Path()], path: click.Path()):
    """Python script dependency analizer.

    Simple program that analyzes ``path``, either Python script or package,
    and builds its dependency graph using Dot.

    Parameters
    ----------
    out : str
        asasa

    paths : List[str]
        asasa

    path : str
        aaddad
    """
    """
    :param out: Dependency graph output.
    :type out: str
    :param paths: List of paths which may contain external dependencies of the
                  target script which will be analized.
    :type paths: List[click.Path()]
    :param path: Path to the Python script to be analyzed.
    :type path: click.Path()
    """
    search_paths = []
    failed_paths = []
    if isinstance(paths, list):
        for p in paths:
            if p.exists():
                search_paths.append(Path(p))
                pass
            else:
                failed_paths.append(Path(p))
                pass
            pass
        pass

    if len(failed_paths) > 0:
        click.echo(
            "Warning: the following search paths does not exists:\n{}".format(
                "\n".join(failed_paths)
            )
        )
        pass

    diag = DependencyGraph(
        Path(path),
        search_paths
        # [ "us2_environment", "us2_environment/earth_magneticfield" ]
    )
    digraph = build(diag)
    if out is not None:
        export(digraph, out)
        # export(digraph, "diagrams/diag")
        pass
    else:
        graph = Dot()
        graph.add_subgraph(digraph)
        click.echo(graph.to_string())
        pass
    pass


cli()
