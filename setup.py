import setuptools

setuptools.setup(
    name="dependency-graph",
    version="0.1.0",
    author="Imanol Sardón",
    author_email="sardondelgadoimanol@gmail.com",
    packages=["dependency_graph"],
    url="https://gitlab.com/personal634/DependencyGraph",
    license="LICENSE.txt",
    description="Python Dependency Tree Graph Generator.",
    long_description=open('README.md').read(),
    install_requires=[
        "pydotplus >= 2.0.2",
        "click >= 7.0"
    ],
    keywords=[
        "Python",
        "Graph"
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Software Development :: Documentation",
        "Topic :: Software Development :: Libraries",
        "Topic :: Software Development :: Libraries :: Python Modules"
    ]
)
